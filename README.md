
# MARCO GEOESTADÍSTICO NACIONAL DE COLOMBIA 2018 

Descargado de
https://www.dane.gov.co/files/geoportal-provisional/index.html
el 9.Feb.2022

Supe de ese URL por [esta respuesta](https://gitlab.com/pasosdeJesus/division-politica/-/blob/main/Colombia/2020/correcciones/respuesta_derecho_de_peticion_Dic_2021/20222450000051T_R_VTAMARA_04012022.pdf) 
a un derecho de petición que envié al DANE.

| Descripción | Versión | Formato | Tamaño | Archivo |
|---|---|---|---|---|
| Presentación Marco Geoestadístico Nacional 2018| 2022| PDF| 6 MBYTES| [220207-MGN2018press.pdf](220207-MGN2018press.pdf) |
| Comunicado de Prensa. Actualización del Marco Geoestadístico Nacional (MGN) 2018 | 2022 | PDF | 1 MBYTES| [Comunicado_prensa_CambiomarcomuestralGEIH_DIG.pdf](Comunicado_prensa_CambiomarcomuestralGEIH_DIG.pdf) |
| Marco geoestadístico nacional MGN2018 | 2018 | ShapeFile | 2.9 GB | [SHP_MGN2018_COLOMBIA.zip](SHP_MGN2018_COLOMBIA.zip) |
| Marco geoestadístico nacional MGN2005 | 2005 | ShapeFile | 2.9 GB | [SHP_MGN2005_COLOMBIA.zip](SHP_MGN2005_COLOMBIA.zip) |
| Manual de uso del MGN2005 | 2005 | ShapeFile | 1 MBYTES | [Manual_MGN.zip](Manual_MGN.zip) |
| Catálogo de objetos MGN 2020 (Diccionario de datos) | 2020 | PDF | 1 MBYTES | [CO_MGN2020.pdf](CO_MGN2020.pdf) |
| Codificación de división político-administrativa de Colombia (Divipola) | 2021 - Dic | Excel | 0.7 MBYTES | [Listados_DIVIPOLA.zip](Listados_DIVIPOLA.zip) |
| Déficit habitacional por manzanas | 2018 | ShapeFile | 250 MBYTES | [SHP_DEFHABXMZ_2018.zip](SHP_DEFHABXMZ_2018.zip) |
| Nivel de referencia de veredas | 2020 | ShapeFile | 221 MBYTES | [SHP_CRVEREDAS_2020.zip](SHP_CRVEREDAS_2020.zip) |
| Directorio estadístico de empresas | 2021-I | Txt | 137 MBYTES | [DIRECTORIO_EMPRESAS_2021_I.zip](DIRECTORIO_EMPRESAS_2021_I.zip) |
| Contraste de estructuras e indicadores demográficos básicos CNPV2018-REBP | 2018-2021 | Excel | 12 MBYTES | [INDICADORES_INSUMOS_REBP_CNPV2018-2020.zip](INDICADORES_INSUMOS_REBP_CNPV2018-2020.zip) |
| Nivel de vulnerabilidad e IPM por manzanas | 2018 | ShapeFile | 249 MBYTES | [SHP_VULNRB_IPMxMZ.zip](SHP_VULNRB_IPMxMZ.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Departamentos| 2018 | ShapeFile | 10 MBYTES | [SHP_MGN2018_INTGRD_DEPTO.zip](SHP_MGN2018_INTGRD_DEPTO.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Municipios | 2018 | ShapeFile | 20 MBYTES | [SHP_MGN2018_INTGRD_MPIO.zip](SHP_MGN2018_INTGRD_MPIO.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Clase Censales | 2018 | ShapeFile | 20 MBYTES | [SHP_MGN2018_INTGRD_CLASECS.zip](SHP_MGN2018_INTGRD_CLASECS.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Sector Rural | 2018 | ShapeFile| 20 MBYTES | [SHP_MGN2018_INTGRD_SECTR.zip](SHP_MGN2018_INTGRD_SECTR.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Sector Urbanos | 2018 | ShapeFile | 30 MBYTES | [SHP_MGN2018_INTGRD_SECTU.zip](SHP_MGN2018_INTGRD_SECTU.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Sección Urbanas | 2018 | ShapeFile | 70 MBYTES | [SHP_MGN2018_INTGRD_SECCU.zip](SHP_MGN2018_INTGRD_SECCU.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Sección Rural | 2018 | ShapeFile | 70 MBYTES | [SHP_MGN2018_INTGRD_SECCR.zip](SHP_MGN2018_INTGRD_SECCR.zip) |
| MGN2018 Integrado con CNPV2018, nivel de Manzana Censales | 2018 | ShapeFile | 400 MBYTES | [SHP_MGN2018_INTGRD_MANZ.zip](SHP_MGN2018_INTGRD_MANZ.zip) |
| MGN2018 Integrado con CNPV2018, zona Censal Urbanas | 2018 | ShapeFile | 24 MBYTES | [SHP_MGN2018_INTGRD_ZCU.zip](SHP_MGN2018_INTGRD_ZCU.zip) |
| MGN2018 Integrado - Instructivo de Uso | 2018 | PDF | 0.5 MBYTES | [MGN2018_Integrado_CNPV2018_InstructivoUso.pdf](MGN2018_Integrado_CNPV2018_InstructivoUso.pdf) |
| Conteo de Unidades Económicas por Departamentos| 2021 | Shapefile | 12 MBYTES | []() |
| Conteo de Unidades Económicas por Municipios | 2021 | Shapefile | 66 MBYTES | [Mpio_conteo.zip](Mpio_conteo.zip) |
| Documento Conteo de Censo Económicos | 2021 | PDF | 1 MBYTES | [Guia_conteo.zip](Guia_conteo.zip) |



Si planeas clonar este repositorio ten en cuenta que require 16G y usa LFS, 
así que antes debes instalar y configurar `git-lfs`.

En [adJ - OpenBSD](https://aprendiendo.pasosdeJesus.org) se hace con:
```
doas pkg_add git-lfs
git lfs install
```

Puedes ver instrucciones para otros sistemas operativos en
<https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/>








